import React, { Component } from 'react';
import PortalProvider from "./components/PortalProvider";
import Portal from "./components/Portal";
import StatefulComponent from "./components/StatefulComponent";
import ComponentWithPortal from "./components/ComponentWithPortal";
import ShowingPortalOnDemand from "./components/ShowingPortalOnDemand";

class App extends Component {
  render() {
    return (
      <PortalProvider>
      <div className="App">
        <div>
          <ComponentWithPortal/>
          <Portal>
            <StatefulComponent/>
          </Portal>
          123
          <Portal>
            <span>Я в портале!</span>
          </Portal>
          321
          <Portal>Ещё один портал!</Portal>
        </div>
        <ShowingPortalOnDemand/>
      </div>
      </PortalProvider>
    );
  }
}

export default App;
