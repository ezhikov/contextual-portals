import React, {createContext, PureComponent} from 'react';
import * as ReactDOM from "react-dom";

export const PortalContext = createContext({});

class PortalProvider extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      portals: new Map()
    };

    this.createPortal = (portal) => {
      const element = document.createElement('div');
      this.setState(({portals: currentPortals}) => {
        document.body.appendChild(element);
        const portals = new Map(currentPortals);
        portals.set(element, portal);
        return {portals};
      });
      return element;
    };

    this.destroyPortal = (element) => {
      document.body.removeChild(element);
      this.setState(({portals: currentPortals}) => {
        const portals = new Map(currentPortals);
        portals.delete(element);
        return {portals};
      });
    };
  }

  render() {
    return (
      <PortalContext.Provider value={{createPortal: this.createPortal, destroyPortal: this.destroyPortal}}>
        {this.props.children}
        {Array.from(this.state.portals.entries()).map(([el, child]) => {
          return ReactDOM.createPortal(child.props.children, el);
        })}
      </PortalContext.Provider>
    );
  }
}

export default PortalProvider;
