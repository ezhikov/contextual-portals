import React, {PureComponent} from 'react';

class StatefulComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      clicks: 0
    }
  }

  render() {
    return (
      <button role="button" onClick={() => this.setState(({clicks}) => ({clicks: clicks + 1}))}>
        Clicks: {this.state.clicks}
      </button>
    );
  }
}

export default StatefulComponent;
