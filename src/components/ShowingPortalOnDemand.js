import React, {PureComponent} from 'react';
import Portal from "./Portal";

class ShowingPortalOnDemand extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    }
  }

  render() {
    return <div>
      <button onClick={() => this.setState(({isOpen}) => ({isOpen: !isOpen}))}>
        {this.state.isOpen ? 'Скрыть портал' : 'Показать портал'}</button>
      {this.state.isOpen && <Portal>Портал, показанный по требованию</Portal>}
    </div>
  }
}

export default ShowingPortalOnDemand
