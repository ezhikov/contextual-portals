import React, {PureComponent} from 'react';
import Portal from "./Portal";

class ComponentWithPortal extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      counter: 0,
      mouseOver: false,
      clicked: false,
    }
  }

  render() {
    return (
      <div
        style={{border: '1px solid #ccc'}}
        onMouseOver={() => this.setState({mouseOver: true})}
        onMouseLeave={() => this.setState({mouseOver: false})}
        onClick={() => this.setState({clicked: true})}
      >
        <div>Счётчик вне портала: {this.state.counter}</div>
        <div>Наведено на родителя: {this.state.mouseOver ? 'Да' : 'Нет'}</div>
        <div>Кликнуто на родителя: {this.state.clicked ? 'Да' : 'Нет'}</div>
        <Portal>
          <button onClick={() => this.setState(({counter}) => ({counter: counter + 1}))}>Кнопка в портале обновляет
            счётчик вне портала
          </button>
        </Portal>
      </div>
    )
  }
}

export default ComponentWithPortal;
