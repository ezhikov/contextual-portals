import React, {PureComponent} from 'react';
import {PortalContext} from "./PortalProvider";

class PortalCreator extends PureComponent {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.portalEl = this.props.createPortal(this);
  }

  componentWillUnmount() {
    this.props.destroyPortal(this.portalEl)
  }

  render() {
    return null;
  };
}

class Portal extends PureComponent {
  render() {
    return (
      <PortalContext.Consumer>
        {({createPortal, destroyPortal}) => (
          <PortalCreator createPortal={createPortal} destroyPortal={destroyPortal}>
            {this.props.children}
          </PortalCreator>
        )}
      </PortalContext.Consumer>
    );
  }
}

export default Portal;
